#ifndef SIM800L_H
#define SIM800L_H

#include <Arduino.h>
#include <SoftwareSerial.h>

#define BAUDRATE 115200

struct CallInfo {
    bool isRinging;
    String ringingNumber;
};

class SIM800L {

public:
    SIM800L(HardwareSerial& uart)
    {
        uart.begin(BAUDRATE);
        delay(1000);
        _uart = &uart;
        _uart->println("AT+CLIP=1");
        _uart->flush();
    }

    SIM800L(SoftwareSerial& uart)
    {
        uart.begin(BAUDRATE);
        delay(1000);
        _uart = &uart;
        _uart->println("AT+CLIP=1");
        _uart->flush();
    }

    CallInfo getCallInfo()
    {
        CallInfo response;
        response.isRinging = false;
        while (_uart->available()) {
            String rx = _uart->readString();
            if (rx.indexOf("RING") > 0) {
                response.isRinging = true;
                while (true) {
                    rx = _uart->readString();
                    if (rx.indexOf("CLIP") > 0) {
                        response.ringingNumber = rx;
                        return response;
                    }
                    delay(100);
                }
            }
            delay(100);
        }
        return response;
    }

    void call(uint16 countryCode, String number)
    {
        hangup();
        delay(1000);
        String callCommand = "ATD+" + String(countryCode) + number + ";";
        _uart->println(callCommand);
        _uart->flush();
    }

    void hangup()
    {
        _uart->println("ATH");
        _uart->flush();
    }

    bool sendSms(String number, String message)
    {
        // TODO
        return false;
    }

private:
    Stream* _uart;
};

#endif // SIM800L_H
