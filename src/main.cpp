#include "SIM800L.h"
#include <Arduino.h>
#include <ArduinoOTA.h>
#include <EEPROM.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>

// Enable the ADC to read the VCC input voltage
ADC_MODE(ADC_VCC);

// Get the number of elements of a given array.
#define size(x) ((long)((sizeof(x)) / (sizeof(x[0]))))

// Status LED's //
const uint8 BLUE_LED = 14; // Setting things up in setup()
const uint8 GREEN_LED = 12; // It's alright: the system is working
const uint8 RED_LED = 13; // There was an error during setup()

void turnOn(uint8 pin) { digitalWrite(pin, LOW); }
void turnOff(uint8 pin) { digitalWrite(pin, HIGH); }

// The minimum interval between calls
const uint64 minCallInterval = 300000; // 300 seconds = 5 minutes
uint64 lastCall = 0;

// The time to wait after calling a number before hangup, in other words,
// the (maximum) time the owner's phone will ring
const uint64 timeUntilHangup = 45000; // 45 seconds

// Country code of the cellphone numbers, in this case, Brazil
const uint16 COUNTRY_CODE = 55;

// The phone numbers to call and send messages in case of a movement detection
String numbersToCall[] = { "11912345678", "11912345678" };

// The numbers authorized to control the state of the lights
String authorizedNumbers[] = { "11912345678", "11912345678", "11912345678", "11912345678" };

// The object representing the SIM800L module used;
SIM800L sim(Serial);

bool isNumberAuthorized(String number)
{
    for (int i = 0; i < size(authorizedNumbers); i++) {
        if (number.indexOf(authorizedNumbers[i]) > 0)
            return true;
    }
    return false;
}

void makeTheCalls()
{
    if ((millis() - lastCall) > minCallInterval) {
        for (int i = 0; i < size(numbersToCall); i++) {
            sim.call(COUNTRY_CODE, numbersToCall[i]);
            delay(timeUntilHangup);
        }
        lastCall = millis();
    }
}

void sendTheSms(String message)
{
    for (int i = 0; i < size(numbersToCall); i++) {
        sim.sendSms(numbersToCall[i], message);
    }
}

// Can be a PIR or a Radar Sensor
struct MotionSensor {
    String name;
    uint8 pin;
    bool motionDetected() { return digitalRead(pin); }
};

MotionSensor sensors[] = { { "Front Door", 9 }, { "Backyard", 10 }, { "Garage", 11 } };

// The pin connected to a switch to turn on and off the system calls due to sensors detection
const uint8 sensorSwitch = 4;

#define ON true
#define OFF false

struct Light {
    String name;
    uint8 pin;
    bool getState() { return !(digitalRead(pin)); }
    void setState(bool state)
    {
        digitalWrite(pin, !state);
        EEPROM.write(pin, state);
        EEPROM.commit();
    }
    void turnOn() { setState(ON); }
    void turnOff() { setState(OFF); }
    void toggle() { setState(!getState()); }
};

Light lights[] = { { "Backyard", 15 } };

int getLightIndexById(char id)
{
    id = toupper(id);
    for (int i = 0; i < size(lights); i++) {
        if (lights[i].name.charAt(0) == id)
            return i;
    }
    return -1;
}

void setupIO()
{
    pinMode(BLUE_LED, OUTPUT);
    turnOn(BLUE_LED);
    pinMode(RED_LED, OUTPUT);
    turnOff(RED_LED);
    pinMode(GREEN_LED, OUTPUT);
    turnOff(GREEN_LED);
    EEPROM.begin(4096);
    for (int i = 0; i < size(lights); i++) {
        pinMode(lights[i].pin, OUTPUT);
        lights[i].setState(EEPROM.read(lights[i].pin));
    }
    pinMode(sensorSwitch, INPUT);
    for (int i = 0; i < size(sensors); i++) {
        pinMode(sensors[i].pin, INPUT);
    }
}

struct RemoteLight {
    String name;
    String IP;
    bool getState()
    {
        HTTPClient http;
        String url = "http://" + IP + "/light";
        http.begin(url);
        return http.GET();
    }
    void setState(bool state)
    {
        HTTPClient http;
        String url = "http://" + IP + "/light";
        String payload;
        if (state)
            payload = "?state=on";
        else
            payload = "?state=off";
        http.begin(url);
        http.POST(payload);
    }
    void turnOn() { setState(ON); }
    void turnOff() { setState(OFF); }
    void toggle() { setState(!getState()); }
};

RemoteLight rlights[] = { { "Garden", "192.168.4.10" } };

const char* SSID = "coruja";
ESP8266WebServer server(80);

void turnOnWiFi()
{
    WiFi.forceSleepWake();
    WiFi.mode(WIFI_AP);
    while (!WiFi.softAP(SSID)) {
        delay(100);
    }

    server.on("/", []() {
        String response = "<h1>Coruja Host</h1>\n";
        response += String(WiFi.softAPgetStationNum());
        response += " stations connected\n\nLights\n";
        for (int i = 0; i < size(lights); i++) {
            response += lights[i].name;
            response += lights[i].getState() ? ": ON\n" : ": OFF\n";
        }
        response += "\nSensors\n";
        for (int i = 0; i < size(sensors); i++) {
            response += sensors[i].name;
            response += sensors[i].motionDetected() ? ": MOVEMENT\n" : ": IDLE\n";
        }
        server.send(200, "text/plain", response);
        server.send(200, "text/plain", response);
    });

    server.on("/light", []() {
        int i = getLightIndexById(server.arg("id").charAt(0));
        if (i == -1) {
            server.send(404, "text/plain", "Light not found");
        } else {
            if (server.hasArg("state")) {
                lights[i].setState(server.arg("state").toInt());
            }
            String response = String(lights[i].name);
            response += lights[i].getState() ? ": ON\n" : ": OFF\n";
            server.send(200, "text/plain", response);
        }
    });

    server.on("/sensor", []() {
        server.send(200);
        if (digitalRead(sensorSwitch)) {
            makeTheCalls();
        }
    });

    // For debugging, only //
    server.on("/rr", []() { server.send(200, "text/plain", ESP.getResetReason()); });
    server.on("/vcc", []() { server.send(200, "text/plain", String(((int)ESP.getVcc()))); });
    // ------------------ //

    server.begin();

    ArduinoOTA.setPort(8266);
    ArduinoOTA.setPassword("coruja");
    ArduinoOTA.begin();
}

void checkSensors()
{
    bool motionDetected = false;
    for (int i = 0; i < size(sensors); i++) {
        if (sensors[i].motionDetected()) {
            motionDetected = true;
            String message = "Motion Detected on " + sensors[i].name;
            sendTheSms(message);
        }
    }
    if (motionDetected) {
        makeTheCalls();
    }
}

void checkForIncomingCall()
{
    CallInfo info = sim.getCallInfo();
    if (info.isRinging) {
        if (isNumberAuthorized(info.ringingNumber)) {
            bool currentState = true;
            // After this loop, the currentState will remain true only if all light are on
            for (int i = 0; i < size(lights); i++) {
                currentState = currentState && lights[i].getState();
            }
            // In the case all light are on, turn all them off, otherwise, turn all them on;
            for (int i = 0; i < size(lights); i++) {
                lights[i].setState(!currentState);
            }
            // Do the same to the remote lights
            for (int i = 0; i < size(rlights); i++) {
                rlights[i].setState(!currentState);
            }
        }
        sim.hangup();
    }
}

void setup(void)
{
    setupIO();
    turnOnWiFi();
    turnOff(BLUE_LED);
    turnOn(GREEN_LED);
}

void loop()
{
    checkForIncomingCall();
    checkSensors();
    server.handleClient();
    ArduinoOTA.handle();
}
